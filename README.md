# Abstract

The increasing complexity of networks, and the need to make them more open
due to the growing emphasis on and attractiveness of the Internet as a medium
for business transactions, mean that networks are becoming more and more
exposed to attacks, both from without and from within. The search is on for
mechanisms and techniques for the protection of internal networks from such
attacks. One of the protective mechanisms under serious consideration is the
firewall. A firewall protects a network by guarding the points of entry to it.
Firewalls are becoming more sophisticated by the day, and new features are
constantly being added, so that, in spite of the criticisms made of them and
developmental trends threatening them, they are still a powerful protective
mechanism.

## Introduction

A **Firewall** is a software program or a piece of hardware that helps to screen out hackers, viruses and worms that try to reach our computer over the Internet.  
Firewall comes in two forms :

- Software firewall

- Hardware firewall

![Hardware and Software Firewall](https://operavps.com/wp-content/uploads/2020/07/software-firewall-vs-hardware-firewall.jpg)

When more than one computer connected to intrusted internet, then it is necessary to protect our computer via hardware firewall as well as software firewall. So if one computer is effected with the virus other will be safe.

## Firewall Basic Approch

Firewall technology can be used to protect networks from hackers, malwares and viruses. there are three basic approaches or services that a firewall uses to protect a network:
packet filtering, circuit proxy, and application proxy.

- ### Packet Filtering

A packet-filtering firewall examines each packet that crosses the firewall and tests the packet according to a set of rules that we set up. If the packet passes the test, then it will only allowed to pass. If the packet doesn’t pass, it’s rejected. Packet filtering having low security.

- ### Circuit Filtering

Circuit filters restrict access on the basis of host machines (not users) by processing the information found in the TCP and UDP packet headers. This allows administrators to create filters that would, for example, prohibit anyone using Computer A from using FTP to access Computer B.

- ### Application Proxy

Application proxy also known as proxy firewall.A proxy firewall is also a proxy server, but not all proxy servers are proxy firewalls. A proxy server acts as an intermediary between clients and servers. It can cache webpages to reduce bandwidth demands, compress data, filter traffic and detect viruses. A proxy server can also be used to hide user information or to connect to services that would be blocked. On the other hand, a proxy firewall inspects all network traffic to detect and protect against potential threats. It can also detect network intrusion and enforce security policies.

## Firewall Limitations

1. Fire walls cannot protect against what has been authorized.

2. It cannot stop social engineering attacks or an unauthorized user intentionally using their access for unwanted purposes.
3. Firewalls cannot fix poor administrative practices or poorly designed security policies.
4. It cannot stop attacks if the traffic does not pass through them.
5. They are only as effective as the rules they are configured to enforce.

## Conclusions

Firewall play very important role to protect our data, computer system and other personal information. Due to rapid grow of network it is very important to use hardware firewall as well software firewall to minimise loss of data.  
Due to continue change and development, new features are regularly added as the
need arises. If developments follow the present trend, they will continue to combine
configurable access control and authentication mechanisms with their traditional
functions, thus providing more powerful and flexible protection for networks to
make them secure.

## References

- [Firewall - Software and Hardware Explained by Tech Term](https://www.youtube.com/watch?v=aUPoA3MSajUlist=PLBbU9-SUUCwV7Dpk7GI8QDLu3w54TNAA6&index=3)

- [Know about the firewall with Cisco](https://www.cisco.com/c/en_in/products/security/firewalls/what-is-a-firewall.html)

- [Sciencedirect Journal for firewall filtering](https://www.sciencedirect.com/topics/computer-science/filtering-firewall)

- [An Overview of Firewall Technologie by Habtamu Abie](https://www.researchgate.net/publication/2371491_An_Overview_of_Firewall_Technologies)
- [Firewall capability and limitation by Ques10](https://www.ques10.com/p/11101/what-is-a-firewall-what-are-the-capabilities-and-3/?)
